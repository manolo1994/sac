<?php
header('Access-Control-Allow-Origin: *');   
session_start();

$user_name = $_SESSION['usuario'];
$password = $_SESSION['password'];
$nombre1 = $_POST['nombre1'];
$nombre2 = $_POST['nombre2'];
$apellido1 = $_POST['apellido1'];
$apellido2 = $_POST['apellido2'];
$cedula = $_POST['cedula'];
$telefono = $_POST['telefono'];
$tipoU = $_POST['tipoU'];
$correo = $_POST['correo'];
$contrasena = $_POST['contrasenia'];

require_once('config.php');

$conectar = ldap_connect("ldap://{$host}:{$port}") or die("No se puede conectar al servidor LDAP");
ldap_set_option($conectar, LDAP_OPT_PROTOCOL_VERSION, 3);
    

    $academico = "ou=".$tipoU.",".$baseAcademicos;
    $administrativos = "ou=".$tipoU.",".$baseAdministrativos;
    $otro = $baseOtros;
    $uid = $correo;

    if($conectar){
        if(@ldap_bind($conectar, "cn={$user_name},{$baseAdmin}", $password)){
            $filtro = "uid=$correo";
            $arreglo = array("userPassword", "ou");
            $resultadoU = @ldap_search($conectar, $baseGeneral, $filtro, $arreglo);
            $entradaA = ldap_get_entries($conectar, $resultadoU);
            for ($i=0; $i<$entradaA["count"]; $i++){
                //@$uid = $entradaA[$i]["uid"][0];
                @$contrasenaA = $entradaA[$i]["userpassword"][0];
                @$ou = $entradaA[$i]["ou"][0];
            }
            if($contrasena == NULL){
                //Si se presenta error toca encriptar también la contraseña. Estar pendiente
                $cambioContra = $contrasenaA;
            }else{
                $hashFormat = "$2y$10$";
                $salt = "cas&ySiGUCAS&LdapCas22";
                $key = $hashFormat.$salt;
                $contra = hash_hmac('sha256', $contrasena, $key, false);
                $cambioContra = $contra;
            } 
            


            if($tipoU == $ou){
                $info["cn"][0] = $nombre1." ".$nombre2;
                $info["sn"][0] = $apellido1." ".$apellido2;
                $info["givenName"][0] = $nombre1." ".$nombre2;
                $info["userPassword"][0] = "$cambioContra";
                $info["telephoneNumber"][0] = "$telefono";
                $info["ou"][0] = "$ou";
                $info["eduPersonTargetedID"][0] = "$cedula";    
                $info["eduPersonPrincipalName"][0] = "$correo";
                $info["mail"][0] = "$correo";
                if($tipoU == 'estudiantes' or $tipoU == 'docentes'){
                    if(@ldap_mod_replace($conectar, "uid={$uid},{$academico}" , $info)){
                        echo "uid";
                    }else{
                        $token = 2;
                        echo "$token";
                    }
                }else{
                    if($tipoU == 'servidores' or $tipoU == 'trabajadores'){
                        if(@ldap_mod_replace($conectar, "uid={$uid},{$administrativos}" , $info)){
                            echo "$uid";
                        }else{
                            $token = 2;
                            echo "$token";
                        }
                    }else{
                        if(@ldap_mod_replace($conectar, "uid={$uid},{$otro}", $info)){
                            echo "$uid";
                        }else{
                            $token = 2;
                            echo "$token";
                        }
                    }
                }
       }else{  
                    $info["cn"][0] = $nombre1." ".$nombre2;
                    $info["sn"][0] = $apellido1." ".$apellido2;
                    $info["givenName"][0] = $nombre1." ".$nombre2;
                     $info["objectclass"][0] = "person";
                     $info["objectclass"][1] = "inetOrgPerson";
                    $info["objectclass"][2] = "organizationalPerson";
                    $info["objectclass"][3] = "top";
                    $info["objectclass"][4] = "eduPerson";
                    $info["userPassword"][0] = "$cambioContra";
                    $info["telephoneNumber"][0] = "$telefono";
                    $info["ou"][0] = "$tipoU";
                    $info["eduPersonTargetedID"][0] = "$cedula";    
                    $info["eduPersonPrincipalName"][0] = "$correo";
                    $info["mail"][0] = "$correo";
                    if($ou == 'estudiantes' or $ou == 'docentes'){
                        $baseAcademicosE = "uid=$uid,ou=$ou,ou=academicos,"."".$baseGeneral;
                        @ldap_delete($conectar, $baseAcademicosE);
                        if($tipoU == 'estudiantes' or $tipoU == 'docentes'){
                                if(@ldap_add($conectar, "uid={$correo},{$academico}" , $info)){
                                    echo "$uid";
                                }else{
                                    $token = 2;
                                    echo "$token";
                                }
                            }else{
                                if($tipoU == 'servidores' or $tipoU == 'trabajadores'){
                                    if(@ldap_add($conectar, "uid={$uid},{$administrativos}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }else{
                                    if(@ldap_add($conectar, "uid={$uid},{$otro}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }
                            }
                    }else{
                        if($ou == 'servidores' or $ou == 'trabajadores'){
                            $baseAdministrativosE = "uid=$uid,ou=$ou,ou=administrativos,"."".$baseGeneral;
                            @ldap_delete($conectar, $baseAdministrativosE);
                            if($tipoU == 'estudiantes' or $tipoU == 'docentes'){
                                if(@ldap_add($conectar, "uid={$uid},{$academico}" , $info)){
                                    echo "$uid";
                                }else{
                                    $token = 2;
                                    echo "$token";
                                }
                            }else{
                                if($tipoU == 'servidores' or $tipoU == 'trabajadores'){
                                    if(@ldap_add($conectar, "uid={$uid},{$administrativos}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }else{
                                    if(@ldap_add($conectar, "uid={$uid},{$otro}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }
                            }
                        }else{
                            $baseOtrosE = "uid=$uid,ou=$ou,"."".$baseGeneral;
                            @ldap_delete($conectar, $baseOtrosE);
                            if($tipoU == 'estudiantes' or $tipoU == 'docentes'){
                                if(@ldap_add($conectar, "uid={$uid},{$academico}" , $info)){
                                    echo "$uid";
                                }else{
                                    $token = 2;
                                    echo "$token";
                                }
                            }else{
                                if($tipoU == 'servidores' or $tipoU == 'trabajadores'){
                                    if(@ldap_add($conectar, "uid={$uid},{$administrativos}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }else{
                                    if(@ldap_add($conectar, "uid={$uid},{$otro}" , $info)){
                                        echo "$uid";
                                    }else{
                                        $token = 2;
                                        echo "$token";
                                    }
                                }
                            }
                        }
                    }    
                }
        
        }else{
            $token = 1;
            echo "$token";
        }

    }else{
        $token = 0;
        echo "$token";
    }
?>