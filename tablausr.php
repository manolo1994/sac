<?php
require_once "coneccion.php";
$prueba = $_GET['id'];
$seleccion = $_GET['id1'];
$datos = usuarios($prueba,$seleccion);
?>


<div class="panel panel-default">
 
  <div class="panel-body"> <?php  if ($datos==0){echo "No existen Usuarios ";}else echo "Existe un total de ".count($datos)." Registros de Usuarios";  ?> </div>

<div class="table-responsive">
<table class="table table-hover">

  <thead class="thead-dark">
    <tr class="bg-primary">
    <td>Cédula <span class="glyphicon glyphicon-credit-card" aria-hidden="true"></span></td>
      <td>Nombre <span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
      <td>Apellido <span class="glyphicon glyphicon-user" aria-hidden="true"></span></td>
      <td>Correo <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></td>
      <td>Teléfono <span class="glyphicon glyphicon-phone" aria-hidden="true"></span></td>
      <td>Usuario <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></td>
      <td>Modificar</td>
      <td>Eliminar</td>
      <td>Vincular</td>
    </tr>
  </thead>
  <tbody>
<?php  


 for ($i=0; $i<count($datos); $i++) {

 	$dtos = $datos[$i][0].'||'.
 			$datos[$i][2].'||'.
 			$datos[$i][1].'||'.
 			$datos[$i][3].'||'.
      $datos[$i][4].'||'.
      $datos[$i][5].'||'.
 			$datos[$i][6];

  ?>

    <tr>
      <td><?php  echo $datos[$i][0]    ?></td>
      <td><?php  echo $datos[$i][2]    ?></td>
      <td><?php  echo $datos[$i][1]    ?></td>
      <td><?php  echo $datos[$i][3]    ?></td>
      <td><?php  echo $datos[$i][4]    ?></td>
      <td><?php  echo $datos[$i][5]    ?></td>
      <td><button type="button" class="btn btn-success glyphicon glyphicon-edit" data-toggle="modal" data-target="#modalactualizar" onclick="agregardatos('<?php echo $dtos  ?>')" ></button> </td>
      <td> <button type="button" class="btn btn-danger glyphicon glyphicon-trash" onclick="eliminarusuario('<?php echo $datos[$i][5] ?>')"></button> </td>
      <td> <button type="button" class="btn btn-warning glyphicon glyphicon-share" data-toggle="modal" data-target="#modalvincular" onclick="selecgrupos('<?php echo $dtos ?>')"></button> </td>
    </tr>
    <?php
    }
    ?>

  </tbody>
</table>
</div>
</div>


